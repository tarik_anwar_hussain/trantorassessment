package com.trantor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Update {

	public static java.sql.Date getCurrentDate() {
		java.util.Date today = new java.util.Date();
		return new java.sql.Date(today.getTime());
	}

	public static void main(String[] args) {
		Connection connection = null;
		PreparedStatement prepareStatement = null;
		try {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/education", "root", "12345");
			prepareStatement = connection.prepareStatement(
					"update customer_details set product_name=?,updated_by = ? ,updated_on=? where cid = ?");
			prepareStatement.setString(1, "anil");
			prepareStatement.setString(2, "rty");
			prepareStatement.setDate(3, getCurrentDate());
			prepareStatement.setInt(4, 1);
			prepareStatement.executeUpdate();
		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {

				e.printStackTrace();
			}
			try {
				prepareStatement.close();
			} catch (SQLException e) {

				e.printStackTrace();
			}

		}
	}

}
