package com.trantor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Insert {

	public static java.sql.Date getCurrentDate() {
		java.util.Date today = new java.util.Date();
		return new java.sql.Date(today.getTime());
	}

	public static void main(String[] args) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/education", "root", "12345");
			preparedStatement = connection
					.prepareStatement("insert into customer_details values(?,?,?,?,?,?,?,?,?,?,?,?)");
			preparedStatement.setInt(1,9);
			preparedStatement.setString(2,"Rajat");
			preparedStatement.setString(3,"Dahiya");
			preparedStatement.setString(4,"RohiniEast");
			preparedStatement.setString(5, "Rohini");
			preparedStatement.setString(6, "Delhi");
			preparedStatement.setInt(7,734562);
			preparedStatement.setString(8,"Colgate");
			preparedStatement.setDate(9, getCurrentDate());
			preparedStatement.setString(10, "abc");
			preparedStatement.setDate(11, getCurrentDate());
			preparedStatement.setString(12, "xyz");

			preparedStatement.executeUpdate();
		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {

				e.printStackTrace();
			}
			try {
				preparedStatement.close();
			} catch (SQLException e) {

				e.printStackTrace();
			}

		}
	}

}





