package com.trantor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DeleteCustomerRecord {

		public static void main(String[] args) {
			Connection connection = null;
			PreparedStatement prepareStatement = null;
			try {
				
				connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/education", "root", "12345");
				prepareStatement = connection.prepareStatement(
						"delete from product_details where cid =1");
				
			} catch (SQLException e) {

				e.printStackTrace();
			} finally {
				try {
					connection.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}
				try {
					prepareStatement.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
		}
	}
